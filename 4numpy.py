#!/usr/bin/env python3

import numpy as np

#NUMPY-$3 BASIC OPERATIONS (TEMEL İŞLEMLER)

##############################################

"""
	• Temel İşlemler Dizilerdeki aritmetik operatörler
	'elementwise' uygulanır.
	Yeni bir dizi oluşturulur ve sonuçla doldurulur.

"""

a = np.array( [20, 30, 40, 50] )
b = np.arange( 4 )

##############################################


##############################################

c = (a - b)
"""
	• .ndarray içerisindeki çıkarma işlemine dikkat edin.
	   b .narray değişkeni içerisindeki değerleri, a .ndarray
 	   değişkeni içerisindeki değerlerden çıkarıyor.

	         [0 1 2 3]
	         [20 29 38 47]

	• 20 den 0 | 30 dan 1 | 40 dan 2 | 50 den 3 çıkarılıyor.

"""
print(c)

##############################################


##############################################
print(b**2)
"""
	• Uygulanan tüm işlemler .ndarray dizisi içerisindeki tüm
	  elemanlara uygulanıyor.

		 [0 1 2 3] >>> b**2 >>> [0 1 4 9]

"""

##############################################


##############################################
print(10*np.sin(a)) # EX.



print(a < 35) # EX.
"""
	>>> [ True  True False False]  Tüm elemanlara uygulanır.

"""

##############################################


##############################################

"""
	• Birçok matris dilinden farklı olarak,
 	  ürün operatörü ' * ' NumPy dizilerinde elementwise çalışır.

 	• Matris ürünü:
	 ' @ ' operatörü veya nokta işlevi veya yöntemi
	  kullanılarak gerçekleştirilebilir:

"""
A = np.array( [[1,1],
			[0,1]] )

B = np.array( [[2,0],
			[3,4]] )

print(A*B)  # elementwise ürünü.

"""
	A dizisi içerisindeki ilk eleman ve
	B dizisi içerisindeki ilk eleman sırasıyla:

		1 * 2 = 2
		1 * 0 = 0
		0 * 3 = 0
		1 * 4 = 4

		Sonuç:
			[[2 0]
 			[0 4]]


"""


#print(A @ B)  # matris ürünü

"""
	EKLENECEK :)
"""

print(A.dot(B)) # bir diğer matris
"""
	EKLENECEK :)
"""

##############################################


##############################################

"""
	• += Ve *= gibi bazı işlemler, yeni bir tane oluşturmak yerine
 	  mevcut bir diziyi değiştirmek için uygulanabilir.

"""

x = np.ones((2,3), dtype=int)
y = np.random.random((2,3))

x *= 3
print(x)

y += x
print(y)

# print(x += y) # b otomatik olarak tamsayı türüne dönüştürülmez
"""
	• İşlem yaparken veri türlerini bilmemiz ve ona göre işlemler
	  yapmamız gerekiyor.
"""

##############################################


##############################################
from math import pi

g = np.ones(3, dtype=np.int32)
"""
	.ones(3 => 3 tane 1 oluşturur.
"""
f = np.linspace(0, pi, 3)
print(f.dtype.name)
h = g+f
print(h)
print(h.dtype.name)



d = np.exp(h*1j)
print(d)
print(d.dtype.name)

##############################################


##############################################

"""
	• Dizideki tüm öğelerin toplamının hesaplanması gibi birçok tek taraflı işlem
	  ndarray sınıfıdır.
"""

k = np.random.random((2,3))
print(k)
print(k.sum())
print(k.min())
print(k.max())


##############################################


##############################################

"""
	• Varsayılan olarak, bu işlemler dizisine, şekline bakılmaksızın
	  bir sayı listesi gibi uygulanır. Ancak, axis parametresini
	  belirterek bir dizinin belirtilen ekseni boyunca bir işlem
	  uygulayabilirsiniz:
"""

n = np.arange(12).reshape(3, 4)
print(n)

print(n.sum(axis=0)) # Her sütunun toplamı
"""
	[[ 0  1  2  3]
 	[ 4  5  6  7]
 	[ 8  9 10 11]]

 	Sırasıyla sütunlar toplanır:

 	0 + 4 + 8 = 12 | 1 + 5 + 9 = 15 | 2 + 6 + 10 = 18 | 3 + 7 + 11 = 21

	Sonuç:
 		[12 15 18 21]

"""
print(n.min(axis=1)) # Her satırın minumumu


print(n.cumsum(axis=1)) # Her satır boyunca genel toplam.
"""
	[[ 0  1  2  3]
 	[ 4  5  6  7]
 	[ 8  9 10 11]]

 	Sırasıyla:

 	0 = 0
 	0 + 1 = 1
 	0 + 1 + 2 = 3
 	0 + 1 + 2 + 3 = 6
	
	4 = 4
	4 + 5 = 9
	4 + 5 + 6 = 15
	4 + 5 + 6 + 7 = 22

	8 = 8
	8 + 9 = 17
	8 + 9 + 10 = 27
	8 + 9 + 10 + 11 = 38

	Sonuç:
		[[ 0  1  3  6]
 		[ 4  9 15 22]
 		[ 8 17 27 38]]

"""

##############################################