#!/usr/bin/env python3

import numpy as np 


#NUMPY-$1

#####################################
a = np.arange(15).reshape(3, 5) 

"""
	• Bu satırda np.arange() fonksiyonu ile 15'e kadar bir sınır belirtiyoruz ve reshape ile 
	  3 satırlık ve 5 sütünluk bir liste oluşturmasını sağlıyoruz.

	• arange() içine girilen değer reshape() içine girilen değerlerin çarpımına eşit olması gerekmektedir.

	• Mesela np.arange(20).reshape(3, 2) yapmak bize hata döndürecektir.
		>>> ValueError: cannot reshape array of size 20 into shape (3,2)

	#ÖZET np.arange(x).reshape(a, b) ==> a*b = x 

"""

#####################################
a.shape

"""
	• .shape fonksiyonu ise bu dizinin kaç satır ve kaç sütun dan oluştuğunu
	 bize döndürüyor.

	• Mesela a değişkenini ekrana bastırmaya çalışsak şöyle bir çıktı alırız:

		array([[ 0,  1,  2,  3,  4],
       		   [ 5,  6,  7,  8,  9],
       	   	   [10, 11, 12, 13, 14]])

	• Görüldüğü üzere .shape ile bu dizinin 3 satırdan ve 5 sütundan oluştuğunu anlayabiliriz.

"""

#####################################


#####################################
a.ndim

"""
	EKLENECEK :)


"""

#####################################



#####################################
a.dtype.name

"""
	• Veri tipini gösterir.
		'int64'
"""
#####################################



#####################################
a.itemsize

"""
	• .itemsize dizinin her elemanının bayt cinsinden boyutudur.
	   Örneğin, float64 tipi bir dizi elemanın 8 (= 64/8) öğesi boyutundayken,
	   complex32 türünden biri öğe 4'e (= 32/8) sahiptir.
"""

#####################################



#####################################
a.size

"""
	• Dizi içerisinde kaç tane eleman bulunduğunu bize döndürür.

"""
#####################################


#####################################
type(a)

"""
	<type 'numpy.ndarray'> tipi bu şekildedir.
	
"""
#####################################



#####################################
b = np.array([6, 7, 8])

"""
	• [6, 7, 8] dizisini .array() fonksiyonu ile ndarray veri tipine
	  dönüştürüyoruz ki numpy kütüphanesinden yararlanabilelim.

"""
#####################################



#####################################
type(b)

"""
<type 'numpy.ndarray'>

"""
#####################################