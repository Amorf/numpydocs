#!/usr/bin/env python3

import numpy as np

#NUMPY-$3 Printing Arrays (DİZİLERİ BASTIRMA)


##############################################

"""

	• Bir diziyi yazdırdığınızda, NumPy iç içe geçmiş listelerle aynı şekilde,
	  ancak aşağıdaki düzende görüntüler:

	• son eksen soldan sağa doğru yazdırılıyorsa,
	• ikinci-sonuncusu yukarıdan aşağıya doğru
	• Gerisi, her dilim birbirinden boş bir satır
	  ile ayrılarak, üstten alta da yazdırılır.

"""

a = np.arange(6)  # 1d array
print(a)


b = np.arange(12).reshape(4, 3) # 2d array
print(b)


c = np.arange(24).reshape(2, 3, 4)  # 3d array
print(c)

##############################################



##############################################

"""
• Yeniden şekillendirme hakkında daha fazla bilgi almak için aşağıya bakın.
• Bir dizi yazdırılamayacak kadar büyükse,
  NumPy otomatik olarak dizinin orta kısmını atlar
  ve yalnızca köşeleri yazdırır.

"""

print(np.arange(10000))

# >>> [   0    1    2 ... 9997 9998 9999]

print(np.arange(10000).reshape(100, 100))

"""
>>> [[   0    1    2 ...   97   98   99]
 	[ 100  101  102 ...  197  198  199]
 	[ 200  201  202 ...  297  298  299]
 	...
 	[9700 9701 9702 ... 9797 9798 9799]
 	[9800 9801 9802 ... 9897 9898 9899]
 	[9900 9901 9902 ... 9997 9998 9999]]
"""

##############################################



##############################################
"""
	• Bu davranışı devre dışı bırakmak ve NumPy'nin
      tüm diziyi yazdırmasını zorlamak için,
"""

np.set_printoptions(threshold=np.nan)

print(np.arange(10000).reshape(100, 100))


##############################################